package com.devcamp.stringlengthapi;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class lengthController {
    @GetMapping("/length")
    public int lengthString(@RequestParam(required = true, name = "string") String len) {
        return len.length();
    }

}
